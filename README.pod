=head1 NAME

countdown starts a gtk2-gui for a countdown

=head1 DESCRIPTION

this program lets you start a countdown timer.

=head1 SYNOPSIS

countdown [options]

(there are no mandatory options, only mandatory sub-options)

general options:

 -e, --exit                    exit on reaching zero
 -f, --format=s                time format to use for displaying
 -s, --start                   immediately start the countdown
     --step=f                  time step to diplay
 -t, --time=i                  don't change anything, just print possible changes

meta options:

 -V, --version                 display version and exit.
 -h, --help                    display brief help
     --man                     display long help (man page)
 -q, --silent                  same as --verbose=0
 -v, --verbose                 same as --verbose=1 (default)
 -vv,--very-verbose            same as --verbose=2
 -v, --verbose=x               grade of verbosity
                                x=0: no output
                                x=1: default output
                                x=2: much output

=head1 EXAMPLES

countdown
  starts the countdown gui with default start value

countdown --time=70
  starts the countdown gui with 70 seconds as start value

countdown -st 70
  starts the countdown gui with 70 seconds as start value and starts the timer

=head1 OPTIONS

=head2 GENERAL

=over 8

=item B<-e>, B<--exit>

=item B<-f>, B<--format>=I<format>

set time format to use for displaying

exit on reaching zero

=item B<-s>, B<--start>

immediately start the countdown

=item B<--step>=I<float>

use this time step in seconds in display

=item B<-t>, B<--time>=I<integer>

set start value in seconds, default = 60

=back

=head2 META

=over 8

=item B<--version>, B<-V>

prints version and exits.

=item B<--help>, B<-h>, B<-?>

prints a brief help message and exits.

=item B<--man>

prints the manual page and exits.

=item B<--verbose>=I<number>, B<-v> I<number>

set grade of verbosity to I<number>. if I<number>==0 then no output
will be given, except hard errors. the higher I<number> is, the more 
output will be printed. default: I<number> = 1.

=item B<--silent, --quiet, -q>

same as B<--verbose=0>.

=item B<--very-verbose, -vv>

same as B<--verbose=2>. you may use B<-vvv> for B<--verbose=3> a.s.o.

=item B<--verbose, -v>

same as B<--verbose=1>.

=back

=head1 LICENCE

Copyright (c) 2017, seth
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

originally written by seth (see https://github.com/wp-seth/camelbot)

